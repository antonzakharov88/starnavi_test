from rest_framework.decorators import action
from rest_framework.response import Response
from core.api import services
from core.api.serializers import FanSerializer


class LikedMixin:
    @action(detail=True, methods=['post'])
    def like(self, request, pk=None):
        obj = self.get_object()
        services.add_like(obj, request.user)

        return Response()

    @action(detail=True, methods=['post'])
    def unlike(self, request, pk=None):
        obj = self.get_object()
        services.remove_like(obj, request.user)

        return Response()

    @action(detail=True, methods=['get'])
    def get_fans(self, request, pk=None):
        obj = self.get_object()
        fans = services.get_fans(obj)
        serializer = FanSerializer(fans, many=True)

        return Response(serializer.data)

    @action(detail=True, methods=['get'])
    def is_fan(self, request, pk=None):
        obj = self.get_object()
        fan = services.is_fan(obj, request.user)
        # serializer = FanSerializer(fan, many=True)

        return Response(fan)
