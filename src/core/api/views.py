from rest_framework import viewsets
from rest_framework import status
from rest_framework.response import Response

from core.api.serializers import PostSerializer, LikeSerializer, CreateUserSerializer, UserSerializer
from core.api.mixins import LikedMixin

from core.models import Post, User, Like


class UserRegister(viewsets.ModelViewSet):
    serializer_class = CreateUserSerializer
    authentication_classes = []
    permission_classes = []

    def list(self, request, *args, **kwargs):
        return Response({"status": "OK",
                         "response": f"Please register a user"},
                        status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = User.objects.create(username=serializer.data.get('username'))
        user.set_password(serializer.data.get('password'))
        user.save()
        headers = self.get_success_headers(serializer.data)
        return Response({"status": "CREATED",
                         "response": f'You have successfully created user {serializer.data.get("username")}'},
                        status=status.HTTP_201_CREATED, headers=headers)


class UserView(viewsets.ModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    authentication_classes = []
    permission_classes = []


class PostViewSet(LikedMixin, viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    authentication_classes = []
    permission_classes = []

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        post = Post.objects.create(title=serializer.data.get('title'),
                                   description=serializer.data.get('description'),
                                   image=serializer.data.get('image'),
                                   user_id=request.user.id)
        post.save()
        headers = self.get_success_headers(serializer.data)
        return Response({"status": "CREATED",
                         "response": f'You have successfully created post {serializer.data.get("title")}'},
                        status=status.HTTP_201_CREATED, headers=headers)


class LikeView(viewsets.ReadOnlyModelViewSet):
    serializer_class = LikeSerializer
    authentication_classes = []
    permission_classes = []

    def get_queryset(self):
        date_from = self.request.GET.get('date_from')
        date_to = self.request.GET.get('date_to')
        queryset = Like.objects.filter()

        if date_from:
            queryset = queryset.filter(date_created__gte=date_from)
        if date_to:
            queryset = queryset.filter(date_created__lte=date_to)

        return queryset

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        like_counter = 0
        for obj in queryset:
            like_counter += 1
        return Response({"status": "ok", "response": f"There are {like_counter} like's  in a given period"},
                        status=status.HTTP_200_OK, )
