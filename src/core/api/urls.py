from rest_framework.routers import DefaultRouter
from core.api.views import PostViewSet, LikeView, UserRegister, UserView

router = DefaultRouter()

router.register(r'register', UserRegister, basename='register')
router.register(r'user', UserView, basename='user')

router.register(r'posts', PostViewSet, basename='posts')
router.register(r'analytics', LikeView, basename='analytics')


urlpatterns = router.urls
