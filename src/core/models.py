from PIL import Image
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.utils.translation import gettext_lazy as _
from django.utils import timezone


class User(AbstractUser):
    birth_date = models.DateField(null=True, blank=True)
    location = models.CharField(max_length=30, blank=True)
    image = models.ImageField(null=True, default='default.jpg', upload_to='accounts/')
    last_activity = models.DateTimeField(_('last_activity'), null=True)
    last_endpoint = models.TextField(null=True)

    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs)

        img = Image.open(self.image.path)

        if img.height > 300 or img.width > 300:
            output_size = (300, 300)
            img.thumbnail(output_size)
            img.save(self.image.path)


class Like(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='likes')
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveSmallIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    date_created = models.DateTimeField(_('date created'), default=timezone.now)


class Post(models.Model):
    title = models.CharField(max_length=64)
    description = models.TextField(max_length=1024, null=True, blank=True)
    image = models.ImageField(null=True, blank=True, upload_to='covers/')
    user = models.ForeignKey(to=User, related_name='posts', on_delete=models.CASCADE)
    likes = GenericRelation(Like)

    @property
    def total_likes(self):
        return self.likes.count()
