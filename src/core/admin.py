from django.contrib import admin # noqa
from core.models import User, Post, Like
# Register your models here.

admin.site.register(User)
admin.site.register(Post)
admin.site.register(Like)
